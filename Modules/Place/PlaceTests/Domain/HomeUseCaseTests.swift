//
//  HomeUseCaseTests.swift
//  PlaceTests
//
//  Created by Andi Septiadi on 07/12/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Place
class HomeUseCaseTests: XCTestCase {
    static var placeIdInput = 1
    static var placeModel = PlaceModel(placeId: 1, name: "Mountain")
    
    let useCase = HomeInteractorMock()

    func testGetTourismList() throws {
        do {
            let result = try useCase
                .getTourismList()
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, [HomeUseCaseTests.placeModel])
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        do {
            let result = try useCase
                .updateFavoritedTourism(placeId: HomeUseCaseTests.placeIdInput,
                                        model: HomeUseCaseTests.placeModel)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result?.placeId, HomeUseCaseTests.placeIdInput)
            XCTAssertEqual(result?.isFavorited, true)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
}

extension HomeUseCaseTests {
    class HomeInteractorMock: HomeUseCase {
        var functionWasCalled = false
        
        func getTourismList() -> Observable<[PlaceModel]> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext([HomeUseCaseTests.placeModel])
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
            functionWasCalled = false
            
            var aModel = model
            aModel.isFavorited = true
            
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(aModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
