//
//  FavoriteUseCaseTests.swift
//  PlaceTests
//
//  Created by Andi Septiadi on 07/12/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Place
class FavoriteUseCaseTests: XCTestCase {

    static var placeIdInput = 1
    static var placeModel = PlaceModel(placeId: 1, name: "Mountain", isFavorited: true)
    
    let useCase = FavoriteInteractorMock()

    func testGetFavoritedTourismList() throws {
        do {
            let result = try useCase
                .getFavoritedTourismList()
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, [FavoriteUseCaseTests.placeModel])
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        do {
            let result = try useCase
                .updateFavoritedTourism(placeId: FavoriteUseCaseTests.placeIdInput,
                                        model: FavoriteUseCaseTests.placeModel)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result?.placeId, FavoriteUseCaseTests.placeIdInput)
            XCTAssertEqual(result?.isFavorited, false)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }

}

extension FavoriteUseCaseTests {
    class FavoriteInteractorMock: FavoriteUseCase {
        var functionWasCalled = false
        
        func getFavoritedTourismList() -> Observable<[PlaceModel]> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext([FavoriteUseCaseTests.placeModel])
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
            functionWasCalled = false
            
            var aModel = model
            aModel.isFavorited = false
            
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(aModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
