//
//  DetailUseCaseTests.swift
//  PlaceTests
//
//  Created by Andi Septiadi on 07/12/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Place
class DetailUseCaseTests: XCTestCase {

    static var placeIdInput = 1
    static var placeModel = PlaceModel(placeId: 1, name: "Mountain")
    
    let useCase = DetailInteractorMock()

    func testGetTourismList() throws {
        do {
            let result = try useCase
                .getTourismPlace(placeId: DetailUseCaseTests.placeIdInput)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, DetailUseCaseTests.placeModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        do {
            let result = try useCase
                .updateFavoritedTourism(placeId: DetailUseCaseTests.placeIdInput,
                                        model: DetailUseCaseTests.placeModel)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result?.placeId, DetailUseCaseTests.placeIdInput)
            XCTAssertEqual(result?.isFavorited, true)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }

}

extension DetailUseCaseTests {
    class DetailInteractorMock: DetailUseCase {
        var functionWasCalled = false
        
        func getTourismPlace(placeId: Int) -> Observable<PlaceModel?> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(DetailUseCaseTests.placeModel)
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
            functionWasCalled = false
            
            var aModel = model
            aModel.isFavorited = true
            
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(aModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
