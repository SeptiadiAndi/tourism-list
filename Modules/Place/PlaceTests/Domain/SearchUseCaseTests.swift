//
//  SearchUseCaseTests.swift
//  PlaceTests
//
//  Created by Andi Septiadi on 07/12/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Place
class SearchUseCaseTests: XCTestCase {

    static var placeIdInput = 1
    static var searchInput = "Mountain"
    static var placeModel = PlaceModel(placeId: 1, name: "Mountain")
    
    let useCase = SearchInteractorMock()

    func testSearchTourismPlaces() throws {
        do {
            let result = try useCase
                .searchTourismPlaces(searchText: SearchUseCaseTests.searchInput)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            XCTAssert((result?.count ?? 0) > 0)
            XCTAssertEqual(result, [SearchUseCaseTests.placeModel])
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        do {
            let result = try useCase
                .updateFavoritedTourism(placeId: SearchUseCaseTests.placeIdInput,
                                        model: SearchUseCaseTests.placeModel)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            XCTAssertEqual(result?.placeId, SearchUseCaseTests.placeIdInput)
            XCTAssertEqual(result?.isFavorited, true)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }

}

extension SearchUseCaseTests {
    class SearchInteractorMock: SearchUseCase {
        var functionWasCalled = false
        
        func searchTourismPlaces(searchText text: String) -> Observable<[PlaceModel]> {
            functionWasCalled = false
            
            var array: [PlaceModel] = []
            
            if SearchUseCaseTests.placeModel.name.contains(text) {
                array.append(SearchUseCaseTests.placeModel)
            }
            
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(array)
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
            functionWasCalled = false
            
            var aModel = model
            aModel.isFavorited = true
            
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(aModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
