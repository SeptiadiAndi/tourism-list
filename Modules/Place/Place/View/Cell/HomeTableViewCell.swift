//
//  HomeTableViewCell.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import UIKit
import Common

protocol HomeTableViewCellDelegate: AnyObject {
    func homeTableViewCell(didTapFavoriteAt cell: HomeTableViewCell)
}

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UILabel!
    @IBOutlet weak var favoriteButton: FavoriteButton!
    
    weak var delegate: HomeTableViewCellDelegate?
    
    lazy var blurView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .systemThinMaterialDark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 1.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var isFavorited: Bool {
        get {
            return favoriteButton.isFavorited
        }
        set {
            favoriteButton.isFavorited = newValue
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
}

// MARK: - Helper

extension HomeTableViewCell {
    func setupViews() {
        placeNameLabel.numberOfLines = 0
        placeNameLabel.font = UIFont.systemFont(ofSize: 24.0, weight: .light)
        placeNameLabel.textColor = .white
        
        placeAddressLabel.numberOfLines = 0
        placeAddressLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        placeAddressLabel.textColor = .white
        
        infoView.insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: infoView.topAnchor),
            blurView.bottomAnchor.constraint(equalTo: infoView.bottomAnchor),
            blurView.leadingAnchor.constraint(equalTo: infoView.leadingAnchor),
            blurView.trailingAnchor.constraint(equalTo: infoView.trailingAnchor)
        ])
    }
    
    func config(place: PlaceModel) {
        ImageLoader.loadImage(url: place.image,
                              placeholder: nil,
                              for: placeImageView)
        
        placeNameLabel.text = place.name
        placeAddressLabel.text = place.address
        isFavorited = place.isFavorited
    }
}

// MARK: - Actions

extension HomeTableViewCell {
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        delegate?.homeTableViewCell(didTapFavoriteAt: self)
    }
}
