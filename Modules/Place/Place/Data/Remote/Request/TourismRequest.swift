//
//  TourismRequest.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation
import ASCore
import Common

public struct TourismRequest: ASRequestInterface {
    public var baseURL: String = Constant.baseURL
    public var path: String = "/list"
    
    public init(baseURL: String = Constant.baseURL, path: String = "/list") {
        self.baseURL = baseURL
        self.path = path
    }
}
