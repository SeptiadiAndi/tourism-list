//
//  TourismResponse.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation

public struct TourismResponse: Decodable {
    var placeId: Int
    var name: String?
    var description: String?
    var address: String?
    var longitude: Double?
    var latitude: Double?
    var like: Int?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case placeId = "id"
        case name
        case description
        case address
        case longitude
        case latitude
        case like
        case image
    }
}
