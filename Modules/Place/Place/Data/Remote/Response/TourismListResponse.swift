//
//  TourismListResponse.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation

struct TourismListResponse: Decodable {
    var error: Bool
    var message: String?
    var count: Int?
    var places: [TourismResponse]?
}
