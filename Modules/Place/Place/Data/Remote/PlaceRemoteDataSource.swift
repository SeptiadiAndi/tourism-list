//
//  PlaceRemoteDataSource.swift
//  Place
//
//  Created by Andi Septiadi on 02/12/21.
//

import Foundation
import ASCore
import RxSwift
import Common

public struct PlaceRemoteDataSource: ASRemoteDataSource {
    public typealias Request = ASRequestInterface
    public typealias Response = Observable<[TourismResponse]>
    
    public init() {}
    
    public func execute(request: ASRequestInterface) -> Observable<[TourismResponse]> {
        return Observable.create { observer -> Disposable in
            Network.request(request: request,
                            type: TourismListResponse.self) { result in
                switch result {
                case .success(let response):
                    observer.onNext(response.places ?? [])
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}
