//
//  TourismRepository.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation
import RxSwift
import ASCore

public protocol TourismRepositoryProtocol {
    func getTourismList() -> Observable<[PlaceModel]>
    func updateFavoritedPlace(placeId: Int, place: PlaceModel) -> Observable<PlaceModel>
    func getFavoritedTourismList() -> Observable<[PlaceModel]>
    func searchTourismPlaces(searchText text: String) -> Observable<[PlaceModel]>
    func getTourismPlace(id: Int) -> Observable<PlaceModel?>
}

public class TourismRepository: NSObject {
    
    public typealias TourismInstance = (PlaceLocalDataSource,
                                        PlaceRemoteDataSource,
                                        ListPlaceMapper) -> TourismRepository
    
    fileprivate let disposeBag: DisposeBag = DisposeBag()
    fileprivate let remote: PlaceRemoteDataSource
    fileprivate let local: PlaceLocalDataSource
    fileprivate let mapper: ListPlaceMapper
    
    private init(local: PlaceLocalDataSource, remote: PlaceRemoteDataSource, mapper: ListPlaceMapper) {
        self.local = local
        self.remote = remote
        self.mapper = mapper
    }
    
    public static let shared: TourismInstance = { localRepo, remote, mapper in
        return TourismRepository(local: localRepo, remote: remote, mapper: mapper)
    }
}

extension TourismRepository {
    private func placesObservable() -> Observable<[PlaceEntity]> {
        return Observable.create { observer -> Disposable in
            self.local.list(request: nil) { result in
                switch result {
                case .success(let list):
                    observer.onNext(list)
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    private func addPlaceObservable(places: [PlaceEntity]) -> Observable<Bool> {
        return Observable.create { observer -> Disposable in
            self.local.add(entities: places) { result in
                switch result {
                case .success(let isSuccess):
                    observer.onNext(isSuccess)
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    private func handleRemoteTourismList(entities: [PlaceEntity]) -> Observable<[PlaceModel]> {
        if entities.isEmpty {
            return self.remote.execute(request: TourismRequest())
                .map { self.mapper.transformResponseToEntity(response: $0) }
                .flatMap({ entities in
                    self.addPlaceObservable(places: entities)
                        .flatMap { _ in
                            self.placesObservable()
                                .map { entities in
                                    self.mapper.transformEntityToDomain(entity: entities)
                                }
                        }
                })
                .catch { _ in
                    self.placesObservable()
                        .map { entities in
                            self.mapper.transformEntityToDomain(entity: entities)
                        }
                }
        } else {
            return self.placesObservable()
                .map { entities in
                    self.mapper.transformEntityToDomain(entity: entities)
                }
        }
    }
}

extension TourismRepository: TourismRepositoryProtocol {
    public func getTourismList() -> Observable<[PlaceModel]> {
        return placesObservable()
            .flatMap { entities in
                self.handleRemoteTourismList(entities: entities)
            }
    }
    
    public func updateFavoritedPlace(placeId: Int, place: PlaceModel) -> Observable<PlaceModel> {
        return Observable.create { observer -> Disposable in
            let aPlace = self.mapper.transformDomainToEntity(domain: [place]).first!
            aPlace.isFavorited = !aPlace.isFavorited
            
            self.local.update(id: placeId, entity: aPlace) { result in
                switch result {
                case .success:
                    if let model = self.mapper.transformEntityToDomain(entity: [aPlace]).first {
                        observer.onNext(model)
                    }
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    public func getFavoritedTourismList() -> Observable<[PlaceModel]> {
        return Observable.create { observer -> Disposable in
            self.local.list(request: nil) { result in
                switch result {
                case .success(let list):
                    let filtered = list.filter { $0.isFavorited }
                    let array = self.mapper.transformEntityToDomain(entity: filtered)
                    observer.onNext(array)
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    public func searchTourismPlaces(searchText text: String) -> Observable<[PlaceModel]> {
        return Observable.create { observer -> Disposable in
            self.local.list(request: nil) { result in
                switch result {
                case .success(let list):
                    let filtered = list.filter { $0.name.contains(text) }
                    let array = self.mapper.transformEntityToDomain(entity: filtered)
                    observer.onNext(array)
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    public func getTourismPlace(id: Int) -> Observable<PlaceModel?> {
        return Observable.create { observer -> Disposable in
            self.local.get(id: String(id)) { result in
                switch result {
                case .success(let entity):
                    if let domain = self.mapper.transformEntityToDomain(entity: [entity]).first {
                        observer.onNext(domain)
                    }
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            return Disposables.create()
        }
    }
}
