//
//  PlaceLocalDataSource.swift
//  Place
//
//  Created by Andi Septiadi on 02/12/21.
//

import Foundation
import ASCore
import RealmSwift

public struct PlaceLocalDataSource: ASLocalDataSource {
    public typealias Request = Any
    public typealias Response = PlaceEntity
    
    private let realm: Realm
    
    public init(realm: Realm) {
        self.realm = realm
    }
    
    public func list(request: Any?, completionHandler: @escaping (Result<[PlaceEntity], Error>) -> Void) {
        let array = Array(realm.objects(PlaceEntity.self))
        completionHandler(.success(array))
    }
    
    public func add(entities: [PlaceEntity], completionHandler: @escaping (Result<Bool, Error>) -> Void) {
        do {
            try realm.write {
                realm.add(entities, update: .modified)
                completionHandler(.success(true))
            }
        } catch let error {
            completionHandler(.failure(error))
        }
    }
    
    public func get(id: String, completionHandler: @escaping (Result<PlaceEntity, Error>) -> Void) {
        guard let place = realm.objects(PlaceEntity.self).filter("placeId = %d", Int(id)!).first else {
            completionHandler(.failure(ASDatabaseError.requestFailed))
            return
        }
        
        completionHandler(.success(place))
    }
    
    public func update(id: Int, entity: PlaceEntity, completionHandler: @escaping (Result<Bool, Error>) -> Void) {
        guard let place = realm.objects(PlaceEntity.self).filter("placeId = %d", id).first else {
            completionHandler(.failure(ASDatabaseError.requestFailed))
            return
        }
        
        do {
            try realm.write {
                place.isFavorited = !place.isFavorited
                completionHandler(.success(true))
            }
        } catch let error {
            completionHandler(.failure(error))
        }
    }
}
