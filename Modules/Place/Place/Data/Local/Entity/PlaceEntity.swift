//
//  PlaceEntity.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation
import RealmSwift

public class PlaceEntity: Object {
    @Persisted var placeId: Int = 0
    @Persisted var name: String = ""
    @Persisted var desc: String = ""
    @Persisted var address: String = ""
    @Persisted var longitude: Double = 0.0
    @Persisted var latitude: Double = 0.0
    @Persisted var like: Int = 0
    @Persisted var image: String = ""
    @Persisted var isFavorited: Bool = false
    
    public override class func primaryKey() -> String? {
        return "placeId"
    }
}
