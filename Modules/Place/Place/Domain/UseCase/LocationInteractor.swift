//
//  LocationInteractor.swift
//  Place
//
//  Created by Andi Septiadi on 01/12/21.
//

import Foundation
import RxSwift

public protocol LocationUseCase {
    func place(id: Int) -> Observable<PlaceModel?>
}

public class LocationInteractor: LocationUseCase {
    private let repository: TourismRepositoryProtocol
    
    public required init(repository: TourismRepositoryProtocol) {
        self.repository = repository
    }
    
    public func place(id: Int) -> Observable<PlaceModel?> {
        return repository.getTourismPlace(id: id)
    }
}
