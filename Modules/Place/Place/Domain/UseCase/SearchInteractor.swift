//
//  SearchInteractor.swift
//  Place
//
//  Created by Andi Septiadi on 26/11/21.
//

import Foundation
import RxSwift

public protocol SearchUseCase {
    func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel>
    func searchTourismPlaces(searchText text: String) -> Observable<[PlaceModel]>
}

public class SearchInteractor: SearchUseCase {
    private let repository: TourismRepositoryProtocol
    
    public required init(repository: TourismRepositoryProtocol) {
        self.repository = repository
    }
    
    public func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
        repository.updateFavoritedPlace(placeId: placeId, place: model)
    }
    
    public func searchTourismPlaces(searchText text: String) -> Observable<[PlaceModel]> {
        return repository.searchTourismPlaces(searchText: text)
    }
}
