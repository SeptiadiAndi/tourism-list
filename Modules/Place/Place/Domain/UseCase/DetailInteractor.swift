//
//  DetailInteractor.swift
//  Place
//
//  Created by Andi Septiadi on 26/11/21.
//

import Foundation
import RxSwift

public protocol DetailUseCase {
    func getTourismPlace(placeId: Int) -> Observable<PlaceModel?>
    func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel>
}

public class DetailInteractor: DetailUseCase {
    private let repository: TourismRepositoryProtocol
    
    public required init(repository: TourismRepositoryProtocol) {
        self.repository = repository
    }
    
    public func getTourismPlace(placeId: Int) -> Observable<PlaceModel?> {
        return repository.getTourismPlace(id: placeId)
    }
    
    public func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
        repository.updateFavoritedPlace(placeId: placeId, place: model)
    }
}
