//
//  HomeInteractor.swift
//  Place
//
//  Created by Andi Septiadi on 26/11/21.
//

import Foundation
import RxSwift

public protocol HomeUseCase {
    func getTourismList() -> Observable<[PlaceModel]>
    func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel>
}

public class HomeInteractor: HomeUseCase {
    private let repository: TourismRepositoryProtocol
    
    public required init(repository: TourismRepositoryProtocol) {
        self.repository = repository
    }
    
    public func getTourismList() -> Observable<[PlaceModel]> {
        return repository.getTourismList()
    }
    
    public func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
        return repository.updateFavoritedPlace(placeId: placeId, place: model)
    }
}
