//
//  FavoriteInteractor.swift
//  Place
//
//  Created by Andi Septiadi on 26/11/21.
//

import Foundation
import RxSwift

public protocol FavoriteUseCase {
    func getFavoritedTourismList() -> Observable<[PlaceModel]>
    func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel>
}

public class FavoriteInteractor: FavoriteUseCase {
    private let repository: TourismRepositoryProtocol
    
    public required init(repository: TourismRepositoryProtocol) {
        self.repository = repository
    }
    
    public func getFavoritedTourismList() -> Observable<[PlaceModel]> {
        return repository.getFavoritedTourismList()
    }
    
    public func updateFavoritedTourism(placeId: Int, model: PlaceModel) -> Observable<PlaceModel> {
        repository.updateFavoritedPlace(placeId: placeId, place: model)
    }
}
