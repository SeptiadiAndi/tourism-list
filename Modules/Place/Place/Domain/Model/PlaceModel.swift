//
//  PlaceModel.swift
//  Place
//
//  Created by Andi Septiadi on 26/11/21.
//

import Foundation

public struct PlaceModel: Equatable {
    let placeId: Int
    let name: String
    var description: String = ""
    var address: String = ""
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    var like: Int = 0
    var image: String = ""
    var isFavorited: Bool = false
}
