//
//  SearchViewController.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import UIKit
import RxSwift
import RxCocoa
import Common

private let kCellId = "CellId"

public class SearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter: SearchPresenterProtocol?
    private var disposeBag = DisposeBag()
    
    lazy var searchBar: UISearchBar = {
        let search: UISearchBar = UISearchBar()
        search.delegate = self
        search.searchTextField.backgroundColor = .white
        search.placeholder = "search_title".localized(bundle: Bundle.placeBundle)
        search.searchTextField.textColor = .black
        return search
    }()
    
    public convenience init(presenter: SearchPresenterProtocol) {
        self.init(nibName: "SearchViewController", bundle: Bundle.placeBundle)
        self.presenter = presenter
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.titleView = searchBar
        navigationItem.leftBarButtonItem = closeBarButtonItem
        
        setupView()
        setupTableView()
        setupDataBinding()
    }
}

// MARK: - Helpers

extension SearchViewController {
    private func setupView() {
        view.backgroundColor = .white
    }
    
    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.rowHeight = UIScreen.main.bounds.size.width * 9 / 16
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: Bundle.placeBundle),
                           forCellReuseIdentifier: kCellId)
    }
    
    private func setupDataBinding() {
        searchBar.rx
            .text
            .orEmpty
            .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { searchText in
                self.presenter?.searchTourismPlaces(searchText: searchText)
            })
            .disposed(by: disposeBag)
        
        presenter?.tourismArray
            .bind(
                to: tableView
                    .rx
                    .items(cellIdentifier: kCellId, cellType: HomeTableViewCell.self)) { _, tourism, cell in
                        cell.config(place: tourism)
                        cell.delegate = self
                    }
                    .disposed(by: disposeBag)
        
        tableView
            .rx
            .modelSelected(PlaceModel.self)
            .subscribe(onNext: { tourism in
                self.searchBar.resignFirstResponder()
                self.presenter?.pushToDetail(with: tourism)
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchBarDelegate {
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - NavigationItemInterface

extension SearchViewController: NavigationItemInterface {}

// MARK: - HomeTableViewCellDelegate

extension SearchViewController: HomeTableViewCellDelegate {
    func homeTableViewCell(didTapFavoriteAt cell: HomeTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        presenter?.updateFavorited(row: indexPath.row)
    }
}
