//
//  SearchPresenter.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import RxSwift
import RxRelay

public protocol SearchPresenterProtocol {
    var tourismArray: BehaviorRelay<[PlaceModel]> { get set }
    
    func searchTourismPlaces(searchText text: String)
    func updateFavorited(row: Int)
    func pushToDetail(with place: PlaceModel)
}

public class SearchPresenter: SearchPresenterProtocol {
    private var useCase: SearchUseCase
    private var router: SearchRouterProtocol
    private var disposeBag = DisposeBag()
    
    public var tourismArray: BehaviorRelay<[PlaceModel]> = BehaviorRelay(value: [])
    
    public init(useCase: SearchUseCase, router: SearchRouterProtocol) {
        self.useCase = useCase
        self.router = router
    }
    
    public func searchTourismPlaces(searchText text: String) {
        useCase.searchTourismPlaces(searchText: text)
            .subscribe(onNext: { places in
                self.tourismArray.accept(places)
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }
    
    public func updateFavorited(row: Int) {
        let tourism = tourismArray.value[row]
        
        useCase.updateFavoritedTourism(placeId: tourism.placeId, model: tourism)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { model in
                var array = self.tourismArray.value
                array[row] = model
                self.tourismArray.accept(array)
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }
    
    public func pushToDetail(with place: PlaceModel) {
        router.pushToDetail(place: place)
    }
}
