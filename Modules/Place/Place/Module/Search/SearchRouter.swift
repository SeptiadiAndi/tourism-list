//
//  SearchRouter.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import ASCore
import RxSwift
import Common

public protocol SearchRouterProtocol: ASRouterInterface {
    func pushToDetail(place: PlaceModel)
}

public class SearchRouter: SearchRouterProtocol {
    public init() {}
    
    public func pushToDetail(place: PlaceModel) {
        guard let detailViewController = Injection
                .shared
                .assembler
                .resolver
                .resolve(DetailViewController.self,
                         argument: place)
        else { return }
        
        present(viewController: detailViewController, presentType: .push)
    }
}
