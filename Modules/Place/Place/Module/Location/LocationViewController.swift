//
//  LocationViewController.swift
//  Place
//
//  Created by Andi Septiadi on 01/12/21.
//

import UIKit
import RxSwift
import Common
import MapKit

public class LocationViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    private var disposeBag = DisposeBag()
    private var presenter: LocationPresenterProtocol?

    public convenience init(placeId: Int, presenter: LocationPresenterProtocol) {
        self.init(nibName: "LocationViewController", bundle: Bundle.placeBundle)
        self.presenter = presenter
        self.presenter?.set(placeId: placeId)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "location_title".localized(bundle: Bundle.placeBundle)
        navigationItem.leftBarButtonItem = backBarButtonItem
        
        setupView()
        setupDataBinding()
        
        presenter?.getTourismPlace()
    }

}

// MARK: - Helpers

extension LocationViewController {
    private func setupView() {
        view.backgroundColor = .white
    }
    
    private func setupDataBinding() {
        presenter?.tourism
            .subscribe(onNext: { model in
                self.reloadLocation(place: model)
            })
            .disposed(by: disposeBag)
    }
    
    private func reloadLocation(place: PlaceModel?) {
        guard let place = place else { return }
        
        let location = CLLocation(latitude: place.latitude, longitude: place.longitude)
        mapView.centerToLocation(location, regionRadius: 60000)
        
        let region = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: 50000,
            longitudinalMeters: 60000)
        mapView.setCameraBoundary(
            MKMapView.CameraBoundary(coordinateRegion: region),
            animated: true)
        
        let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 200000)
        mapView.setCameraZoomRange(zoomRange, animated: true)
        
        addAnnotation(place: place)
    }
    
    private func addAnnotation(place: PlaceModel) {
        let location = CLLocation(latitude: place.latitude, longitude: place.longitude)
        
        let pin = MKPointAnnotation()
        pin.title = place.name
        pin.subtitle = place.address
        pin.coordinate = location.coordinate
        
        mapView.addAnnotation(pin)
    }
}

// MARK: - NavigationItemInterface

extension LocationViewController: NavigationItemInterface {}

private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 1000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}
