//
//  LocationRouter.swift
//  Place
//
//  Created by Andi Septiadi on 01/12/21.
//

import Foundation
import ASCore

public protocol LocationRouterProtocol: ASRouterInterface {}

public class LocationRouter: LocationRouterProtocol {
    public init() {}
}
