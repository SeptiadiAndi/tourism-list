//
//  LocationPresenter.swift
//  Place
//
//  Created by Andi Septiadi on 01/12/21.
//

import Foundation
import RxSwift
import RxRelay

public protocol LocationPresenterProtocol {
    var tourism: BehaviorRelay<PlaceModel?> { get set }
    
    func set(placeId: Int)
    func getTourismPlace()
}

public class LocationPresenter: LocationPresenterProtocol {
    private var useCase: LocationUseCase
    private var router: LocationRouterProtocol
    private var disposeBag = DisposeBag()
    private var placeId: Int?
    
    public var tourism: BehaviorRelay<PlaceModel?> = BehaviorRelay(value: nil)
    
    public init(useCase: LocationUseCase, router: LocationRouterProtocol) {
        self.useCase = useCase
        self.router = router
    }
    
    public func set(placeId: Int) {
        self.placeId = placeId
    }
    
    public func getTourismPlace() {
        guard let id = placeId else { return }
        
        useCase
            .place(id: id)
            .subscribe(onNext: { model in
                self.tourism.accept(model)
            })
            .disposed(by: disposeBag)
    }
}
