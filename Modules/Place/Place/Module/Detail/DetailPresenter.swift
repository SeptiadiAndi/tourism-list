//
//  DetailPresenter.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import RxSwift
import RxRelay

public protocol DetailPresenterProtocol {
    var tourism: BehaviorRelay<PlaceModel?> { get set }
    
    func setPlaceId(id: Int)
    func getTourismPlace()
    func updateFavorited()
    func pushToLocation()
}

public class DetailPresenter: DetailPresenterProtocol {
    private var useCase: DetailUseCase
    private var router: DetailRouterProtocol
    private var disposeBag = DisposeBag()
    private var placeId: Int?
    
    public var tourism: BehaviorRelay<PlaceModel?> = BehaviorRelay(value: nil)
    
    public init(useCase: DetailUseCase, router: DetailRouterProtocol) {
        self.useCase = useCase
        self.router = router
    }
    
    public func setPlaceId(id: Int) {
        placeId = id
    }
    
    public func getTourismPlace() {
        guard let id = placeId else { return }
        
        useCase
            .getTourismPlace(placeId: id)
            .subscribe(onNext: { model in
                self.tourism.accept(model)
            })
            .disposed(by: disposeBag)
    }
    
    public func updateFavorited() {
        guard let model = tourism.value else { return }
        
        useCase.updateFavoritedTourism(placeId: model.placeId, model: model)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { model in
                self.tourism.accept(model)
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }
    
    public func pushToLocation() {
        router.pushToLocation(placeId: placeId)
    }
}
