//
//  DetailViewController.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import UIKit
import SDWebImage
import RxSwift
import Common

public class DetailViewController: UIViewController {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var favoriteButton: FavoriteButton!
    
    private var disposeBag = DisposeBag()
    private var presenter: DetailPresenterProtocol?
    
    lazy var blurView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .systemThinMaterialDark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 1.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var locationBarItem: UIBarButtonItem = {
        let item = UIBarButtonItem(image: ImageLoader.image(named: "ic-location"),
                                   style: .plain,
                                   target: self,
                                   action: #selector(locationButtonTapped(_:)))
        return item
    }()

    public convenience init(tourism: PlaceModel, presenter: DetailPresenterProtocol) {
        self.init(nibName: "DetailViewController", bundle: Bundle.placeBundle)
        self.presenter = presenter
        self.presenter?.setPlaceId(id: tourism.placeId)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = backBarButtonItem
        navigationItem.rightBarButtonItem = locationBarItem
        
        setupViews()
        setupLabels()
        setupDataBinding()
        
        presenter?.getTourismPlace()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
}

// MARK: - Helpers

extension DetailViewController {
    private func setupViews() {
        view.backgroundColor = .lightColor
        
        infoView.insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: infoView.topAnchor),
            blurView.bottomAnchor.constraint(equalTo: infoView.bottomAnchor),
            blurView.leadingAnchor.constraint(equalTo: infoView.leadingAnchor),
            blurView.trailingAnchor.constraint(equalTo: infoView.trailingAnchor)
        ])
    }
    
    private func setupLabels() {
        nameLabel.font = UIFont.systemFont(ofSize: 28.0, weight: .light)
        nameLabel.textColor = .white
        nameLabel.numberOfLines = 0
        
        addressLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        addressLabel.textColor = .white
        addressLabel.numberOfLines = 0
        
        descLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        descLabel.textColor = .darkTextColor
        descLabel.textAlignment = .justified
    }
    
    private func updateView(tourism: PlaceModel?) {
        guard let tourism = tourism else { return }
        
        placeImageView.sd_setImage(with: URL(string: tourism.image))
        nameLabel.text = tourism.name
        addressLabel.text = tourism.address
        descLabel.text = tourism.description
        favoriteButton.isFavorited = tourism.isFavorited
    }
    
    private func setupDataBinding() {
        presenter?.tourism
            .subscribe(onNext: { tourism in
                self.updateView(tourism: tourism)
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Actions

extension DetailViewController {
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        presenter?.updateFavorited()
    }
    
    @objc func locationButtonTapped(_ sender: UIBarButtonItem) {
        presenter?.pushToLocation()
    }
}

extension DetailViewController: NavigationItemInterface {}
