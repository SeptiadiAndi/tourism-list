//
//  DetailRouter.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import ASCore
import Common

public protocol DetailRouterProtocol: ASRouterInterface {
    func pushToLocation(placeId: Int?)
}

public class DetailRouter: DetailRouterProtocol {
    public init() {}
    
    public func pushToLocation(placeId: Int?) {
        guard let placeId = placeId,
              let viewController = Injection
                .shared
                .assembler
                .resolver
                .resolve(LocationViewController.self,
                         argument: placeId)
        else { return }
        
        present(viewController: viewController, presentType: .push)
    }
}
