//
//  HomeViewController.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import UIKit
import RxSwift
import RxCocoa

private let kCellId = "CellId"

public class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    public var presenter: HomePresenterProtocol?
    
    private var disposeBag: DisposeBag = DisposeBag()
    
    lazy var refreshControl: UIRefreshControl = {
        let control: UIRefreshControl = UIRefreshControl()
        control.tintColor = .white
        return control
    }()
    
    lazy var searchItem: UIBarButtonItem = {
        let item: UIBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .search,
            target: self,
            action: #selector(searchButtonTapped(_:))
        )
        return item
    }()

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "tourism_title".localized(bundle: Bundle.placeBundle)
        navigationItem.rightBarButtonItem = searchItem
        
        setupView()
        setupTableView()
        setupDataBinding()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        
        presenter?.getTourismList()
    }

}

// MARK: - Helpers

public extension HomeViewController {
    private func setupView() {
        view.backgroundColor = .white
    }
    
    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.refreshControl = refreshControl
        tableView.rowHeight = UIScreen.main.bounds.size.width * 9 / 16
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: Bundle.placeBundle),
                           forCellReuseIdentifier: kCellId)
    }
    
    private func setupDataBinding() {
        presenter?.isLoading
            .subscribe(onNext: { _ in
                self.refreshControl.endRefreshing()
            })
            .disposed(by: disposeBag)
        
        presenter?.tourismArray
            .bind(
                to: tableView
                    .rx
                    .items(cellIdentifier: kCellId, cellType: HomeTableViewCell.self)) { _, tourism, cell in
                        cell.config(place: tourism)
                        cell.delegate = self
                    }
                    .disposed(by: disposeBag)
        
        tableView
            .rx
            .modelSelected(PlaceModel.self)
            .subscribe(onNext: { tourism in
                self.presenter?.pushToDetail(with: tourism)
            })
            .disposed(by: disposeBag)
        
        refreshControl
            .rx
            .controlEvent(.valueChanged)
            .subscribe(onNext: { _ in
                self.presenter?.getTourismList()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Actions

public extension HomeViewController {
    @objc func searchButtonTapped(_ sender: UIBarButtonItem) {
        presenter?.presentSearch()
    }
}

// MARK: - HomeTableViewCellDelegate

extension HomeViewController: HomeTableViewCellDelegate {
    func homeTableViewCell(didTapFavoriteAt cell: HomeTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        presenter?.updateFavorited(row: indexPath.row)
    }
}
