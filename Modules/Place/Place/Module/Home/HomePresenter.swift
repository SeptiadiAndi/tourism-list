//
//  HomePresenter.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation
import RxSwift
import RxRelay

public protocol HomePresenterProtocol {
    var tourismArray: BehaviorRelay<[PlaceModel]> { get set }
    var isLoading: BehaviorRelay<Bool> { get set }
    
    func getTourismList()
    func pushToDetail(with place: PlaceModel)
    func updateFavorited(row: Int)
    func presentSearch()
}

public class HomePresenter: HomePresenterProtocol {
    private var disposeBag: DisposeBag = DisposeBag()
    private var useCase: HomeUseCase
    private var router: HomeRouterProtocol
    
    public var tourismArray: BehaviorRelay<[PlaceModel]> = BehaviorRelay(value: [])
    public var isLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    public init(useCase: HomeUseCase, router: HomeRouterProtocol) {
        self.useCase = useCase
        self.router = router
    }
    
    public func getTourismList() {
        isLoading.accept(true)
        useCase.getTourismList()
            .subscribe(onNext: { array in
                self.tourismArray.accept(array)
            }, onError: { error in
                print(error)
            }, onCompleted: {
                self.isLoading.accept(false)
            })
            .disposed(by: disposeBag)
    }
    
    public func pushToDetail(with place: PlaceModel) {
        router.pushToDetail(with: place)
    }
    
    public func updateFavorited(row: Int) {
        let tourism = tourismArray.value[row]
        
        useCase.updateFavoritedTourism(placeId: tourism.placeId, model: tourism)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { model in
                var array = self.tourismArray.value
                array[row] = model
                self.tourismArray.accept(array)
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }

    public func presentSearch() {
        router.presentSearch()
    }
}
