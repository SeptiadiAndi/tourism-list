//
//  HomeRouter.swift
//  Place
//
//  Created by Andi Septiadi on 29/11/21.
//

import UIKit
import ASCore
import Common

public protocol HomeRouterProtocol: ASRouterInterface {
    func pushToDetail(with place: PlaceModel)
    func presentSearch()
}

public class HomeRouter: HomeRouterProtocol {
    public init() {}
    
    public func pushToDetail(with place: PlaceModel) {
        guard let detailViewController = Injection
                .shared
                .assembler
                .resolver
                .resolve(DetailViewController.self,
                         argument: place)
        else { return }
        
        present(viewController: detailViewController, presentType: .push)
    }
    
    public func presentSearch() {
        guard let searchViewController = Injection
                .shared
                .resolve(SearchViewController.self)
        else { return }
        let navController = UINavigationController(rootViewController: searchViewController)
        navController.modalPresentationStyle = .fullScreen
        present(viewController: navController, presentType: .present)
    }
}
