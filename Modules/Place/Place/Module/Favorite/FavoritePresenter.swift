//
//  FavoritePresenter.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import RxSwift
import RxRelay

public protocol FavoritePresenterProtocol {
    var tourismArray: BehaviorRelay<[PlaceModel]> { get set }
    
    func getFavoritedTourismList()
    func updateFavorited(row: Int)
    func pushToDetail(place: PlaceModel)
}

public class FavoritePresenter: FavoritePresenterProtocol {
    private var useCase: FavoriteUseCase
    private var router: FavoriteRouterProtocol
    private var disposeBag = DisposeBag()
    
    public var tourismArray: BehaviorRelay<[PlaceModel]> = BehaviorRelay(value: [])
    
    public init(useCase: FavoriteUseCase, router: FavoriteRouterProtocol) {
        self.useCase = useCase
        self.router = router
    }
    
    public func getFavoritedTourismList() {
        useCase.getFavoritedTourismList()
            .subscribe(onNext: { array in
                self.tourismArray.accept(array)
            })
            .disposed(by: disposeBag)
    }
    
    public func updateFavorited(row: Int) {
        let tourism = tourismArray.value[row]
        
        useCase.updateFavoritedTourism(placeId: tourism.placeId, model: tourism)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { _ in
                var array = self.tourismArray.value
                array.remove(at: row)
                self.tourismArray.accept(array)
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }
    
    public func pushToDetail(place: PlaceModel) {
        router.pushToDetail(place: place)
    }
}
