//
//  FavoriteRouter.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import RxSwift
import Common
import ASCore

public protocol FavoriteRouterProtocol: ASRouterInterface {
    func pushToDetail(place: PlaceModel)
}

public class FavoriteRouter: FavoriteRouterProtocol {
    public init() {}
    
    public func pushToDetail(place: PlaceModel) {
        guard let detailViewController = Injection
                .shared
                .assembler
                .resolver
                .resolve(DetailViewController.self,
                         argument: place)
        else { return }
        
        present(viewController: detailViewController, presentType: .push)
    }
}
