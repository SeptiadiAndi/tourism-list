//
//  FavoriteViewController.swift
//  Place
//
//  Created by Andi Septiadi on 30/11/21.
//

import UIKit
import RxSwift
import RxCocoa

private let kCellId = "CellId"

public class FavoriteViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter: FavoritePresenterProtocol?
    private let disposeBag = DisposeBag()
    
    public convenience init(presenter: FavoritePresenterProtocol) {
        self.init(nibName: "FavoriteViewController", bundle: Bundle.placeBundle)
        self.presenter = presenter
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "favorites_title".localized(bundle: Bundle.placeBundle)
        
        setupTableView()
        setupDataBinding()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.getFavoritedTourismList()
    }
    
}

// MARK: - Helpers

extension FavoriteViewController {
    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.rowHeight = UIScreen.main.bounds.size.width * 9 / 16
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: Bundle.placeBundle),
                           forCellReuseIdentifier: kCellId)
    }
    
    private func setupDataBinding() {
        presenter?.tourismArray
            .bind(
                to: tableView
                    .rx
                    .items(cellIdentifier: kCellId, cellType: HomeTableViewCell.self)) { _, tourism, cell in
                        cell.config(place: tourism)
                        cell.delegate = self
                    }
                    .disposed(by: disposeBag)
        
        tableView
            .rx
            .modelSelected(PlaceModel.self)
            .subscribe(onNext: { tourism in
                self.presenter?.pushToDetail(place: tourism)
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - HomeTableViewCellDelegate

extension FavoriteViewController: HomeTableViewCellDelegate {
    func homeTableViewCell(didTapFavoriteAt cell: HomeTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        presenter?.updateFavorited(row: indexPath.row)
    }
}
