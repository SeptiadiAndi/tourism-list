//
//  ListPlaceMapper.swift
//  Place
//
//  Created by Andi Septiadi on 05/12/21.
//

import Foundation
import ASCore

public struct ListPlaceMapper: ASMapper {
    public typealias Response = [TourismResponse]
    public typealias Entity = [PlaceEntity]
    public typealias Domain = [PlaceModel]
    
    public init() {}
    
    public func transformResponseToEntity(response: [TourismResponse]) -> [PlaceEntity] {
        response.map { aResponse in
            let tourism = PlaceEntity()
            tourism.placeId = aResponse.placeId
            tourism.name = aResponse.name ?? ""
            tourism.desc = aResponse.description ?? ""
            tourism.address = aResponse.address ?? ""
            tourism.longitude = aResponse.longitude ?? 0.0
            tourism.latitude = aResponse.latitude ?? 0.0
            tourism.like = aResponse.like ?? 0
            tourism.image = aResponse.image ?? ""
            return tourism
        }
    }
    
    public func transformEntityToDomain(entity: [PlaceEntity]) -> [PlaceModel] {
        entity.map { aEntity in
            PlaceModel(
                placeId: aEntity.placeId,
                name: aEntity.name,
                description: aEntity.desc,
                address: aEntity.address,
                longitude: aEntity.longitude,
                latitude: aEntity.latitude,
                like: aEntity.like,
                image: aEntity.image,
                isFavorited: aEntity.isFavorited
            )
        }
    }
    
    public func transformDomainToEntity(domain: [PlaceModel]) -> [PlaceEntity] {
        domain.map { aDomain in
            let entity = PlaceEntity()
            entity.placeId = aDomain.placeId
            entity.name = aDomain.name
            entity.desc = aDomain.description
            entity.address = aDomain.address
            entity.longitude = aDomain.longitude
            entity.latitude = aDomain.latitude
            entity.like = aDomain.like
            entity.image = aDomain.image
            entity.isFavorited = aDomain.isFavorited
            return entity
        }
    }
}
