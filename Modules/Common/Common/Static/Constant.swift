//
//  Constant.swift
//  Common
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation

public struct Constant {
    public static let baseURL = "https://tourism-api.dicoding.dev"
    
    public struct Bundle {
        public static let place = "com.septiadi.Place"
        public static let user = "com.septiadi.User"
        public static let common = "com.septiadi.Common"
        public static let menu = "com.septiadi.Menu"
    }
}
