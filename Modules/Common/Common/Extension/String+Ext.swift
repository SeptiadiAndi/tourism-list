//
//  String+Ext.swift
//  Common
//
//  Created by Andi Septiadi on 01/12/21.
//

import Foundation

extension String {
    public func localized(bundle: Bundle?) -> String {
        let aBundle = bundle ?? Bundle.main
        return aBundle.localizedString(forKey: self, value: nil, table: nil)
    }
}
