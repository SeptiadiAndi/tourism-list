//
//  UIColor+Ext.swift
//  Common
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit

public extension UIColor {
    static let mainColor: UIColor = UIColor(
        red: 50.0/255.0,
        green: 80.0/255.0,
        blue: 46.0/255.0,
        alpha: 1.0
    )
    static let lightColor: UIColor = UIColor(
        red: 255.0/255.0,
        green: 248.0/255.0,
        blue: 229.0/255.0,
        alpha: 1.0
    )
    static let darkTextColor: UIColor = UIColor(
        red: 25.0/255.0,
        green: 26.0/255.0,
        blue: 25.0/255.0,
        alpha: 1.0
    )
}
