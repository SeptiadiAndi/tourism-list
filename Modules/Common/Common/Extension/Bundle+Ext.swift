//
//  Bundle+Ext.swift
//  Common
//
//  Created by Andi Septiadi on 01/12/21.
//

import Foundation

public extension Bundle {
    static var placeBundle: Bundle? {
        return Bundle(identifier: Constant.Bundle.place)
    }
    
    static var userBundle: Bundle? {
        return Bundle(identifier: Constant.Bundle.user)
    }
    
    static var commonBundle: Bundle? {
        return Bundle(identifier: Constant.Bundle.common)
    }
    
    static var menuBundle: Bundle? {
        return Bundle(identifier: Constant.Bundle.menu)
    }
}
