//
//  NavigationItemInterface.swift
//  Common
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit

public protocol NavigationItemInterface: AnyObject {
    var backBarButtonItem: UIBarButtonItem { get }
    var closeBarButtonItem: UIBarButtonItem { get }
}

public extension NavigationItemInterface where Self: UIViewController {
    var backBarButtonItem: UIBarButtonItem {
        let item = UIBarButtonItem(
            image: ImageLoader.image(named: "ic-back")?
                .withRenderingMode(.alwaysTemplate),
            style: .plain,
            target: self,
            action: #selector(popViewController(_:))
        )
        return item
    }
    
    var closeBarButtonItem: UIBarButtonItem {
        let item = UIBarButtonItem(
            image: ImageLoader.image(named: "ic-close")?
                .withRenderingMode(.alwaysTemplate),
            style: .plain,
            target: self,
            action: #selector(dismissViewController(_:))
        )
        return item
    }
}

private extension UIViewController {
    @objc func popViewController(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissViewController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
