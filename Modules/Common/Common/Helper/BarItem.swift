//
//  BarItem.swift
//  Common
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit

public struct BarItem {
    public var itemImage: UIImage?
    public var itemTitle: String?
    public var itemTag: Int
    
    public init(itemImage: UIImage? = nil, itemTitle: String? = nil, itemTag: Int) {
        self.itemImage = itemImage
        self.itemTitle = itemTitle
        self.itemTag = itemTag
    }
    
    public var tabBarItem: UITabBarItem {
        let item: UITabBarItem = .init(title: itemTitle, image: itemImage, tag: itemTag)
        item.titlePositionAdjustment = .init(horizontal: 0.0, vertical: -2.5)
        return item
    }
}
