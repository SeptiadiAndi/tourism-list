//
//  Network.swift
//  Common
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation
import Alamofire
import ASCore

public typealias NetworkResult<T: Decodable> = (Swift.Result<T, Error>) -> Void

public protocol NetworkInterface {
    static func request<T: Decodable>(request: ASRequestInterface,
                                      type: T.Type,
                                      completion: @escaping NetworkResult<T>)
}

public class Network: NetworkInterface {
    public static func request<T>(request: ASRequestInterface,
                                  type: T.Type,
                                  completion: @escaping NetworkResult<T>) where T: Decodable {
        guard let url = URL(string: request.url()) else { return }
        
        AF.request(url)
            .validate()
            .responseDecodable(of: type) { response in
                switch response.result {
                case .success(let response):
                    completion(.success(response))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}
