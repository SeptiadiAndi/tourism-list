//
//  ImageLoader.swift
//  Common
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit
import SDWebImage

public class ImageLoader {
    public static func image(named: String) -> UIImage? {
        return UIImage(named: named, in: Bundle.commonBundle, with: nil)
    }
    
    public static func loadImage(url: String, placeholder: UIImage? = nil, for imageView: UIImageView) {
        imageView.sd_setImage(with: URL(string: url),
                              placeholderImage: placeholder,
                              options: .refreshCached,
                              completed: nil)
    }
}
