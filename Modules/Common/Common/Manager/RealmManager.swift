//
//  RealmManager.swift
//  Common
//
//  Created by Andi Septiadi on 02/12/21.
//

import Foundation
import RealmSwift

public class RealmManager {
    public var realm: Realm
    
    public static let shared = RealmManager()
    
    public init() {
        realm = try! Realm()
    }
}
