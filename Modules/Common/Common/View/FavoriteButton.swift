//
//  FavoriteButton.swift
//  Common
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit

public class FavoriteButton: UIButton {
    
    public var isFavorited: Bool = false {
        didSet {
            setupButton()
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        setImage(UIImage(named: "ic-favorite-18")?
                    .withRenderingMode(.alwaysTemplate), for: .normal)
        
        setupButton()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.masksToBounds = true
        layer.cornerRadius = frame.height / 2
    }

    private func setupButton() {
        if isFavorited {
            tintColor = .red
        } else {
            tintColor = .white
        }
    }
    
}
