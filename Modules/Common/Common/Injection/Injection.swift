//
//  Injection.swift
//  Common
//
//  Created by Andi Septiadi on 25/11/21.
//

import Foundation
import Swinject

public class Injection {
    public static let shared: Injection = Injection()
    
    public var assembler: Assembler = Assembler()
    
    public func resolve<Service>(_ serviceType: Service.Type) -> Service? {
        assembler.resolver.resolve(serviceType)
    }
}
