//
//  MainNavigationViewController.swift
//  Common
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit

open class MainNavigationViewController: UINavigationController {
    public convenience init(rootViewController: UIViewController, tabBarItem: UITabBarItem) {
        self.init(rootViewController: rootViewController)
        self.tabBarItem = tabBarItem
    }
}
