//
//  AboutViewController.swift
//  User
//
//  Created by Andi Septiadi on 25/11/21.
//

import UIKit
import Common
import RxSwift

public class AboutViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    private var presenter: AboutPresenter?
    private var disposeBag = DisposeBag()
    
    public convenience init(presenter: AboutPresenter) {
        self.init(nibName: "AboutViewController", bundle: Bundle.userBundle)
        self.presenter = presenter
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupDataBinding()
        
        presenter?.getUserProfile()
    }

}

// MARK: - Helpers

extension AboutViewController {
    private func setupViews() {
        view.backgroundColor = .mainColor
        navigationController?.isNavigationBarHidden = true
        
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.frame.height / 2
        
        nameLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .light)
        nameLabel.textColor = .white
        
        emailLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .light)
        emailLabel.textColor = .white
    }
    
    private func setupDataBinding() {
        presenter?.user
            .subscribe(onNext: { model in
                if let aModel = model {
                    self.updateView(user: aModel)
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func updateView(user: UserModel) {
        imageView.image = user.image
        nameLabel.text = user.name
        emailLabel.text = user.email
    }
}
