//
//  AboutPresenter.swift
//  User
//
//  Created by Andi Septiadi on 25/11/21.
//

import Foundation
import RxSwift
import RxRelay

public class AboutPresenter {
    private var disposeBag: DisposeBag = DisposeBag()
    private var useCase: AboutUseCase
    
    var user: BehaviorRelay<UserModel?> = BehaviorRelay(value: nil)
    
    public init(useCase: AboutUseCase) {
        self.useCase = useCase
    }
    
    func getUserProfile() {
        useCase.getUserProfile()
            .observe(on: MainScheduler.instance)
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: { model in
                self.user.accept(model)
            })
            .disposed(by: disposeBag)
    }
}
