//
//  AboutInteractor.swift
//  User
//
//  Created by Andi Septiadi on 25/11/21.
//

import Foundation
import RxSwift

public protocol AboutUseCase {
    func getUserProfile() -> Observable<UserModel?>
}
public class AboutInteractor: AboutUseCase {
    private let repository: UserRepositoryProtocol
    
    public required init(repository: UserRepositoryProtocol) {
        self.repository = repository
    }
    
    public func getUserProfile() -> Observable<UserModel?> {
        repository.getUser()
    }
}
