//
//  UserModel.swift
//  User
//
//  Created by Andi Septiadi on 29/11/21.
//

import UIKit

public struct UserModel: Equatable {
    public var name: String
    public var email: String
    public var image: UIImage
}
