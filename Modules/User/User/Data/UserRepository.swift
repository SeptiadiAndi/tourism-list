//
//  UserRepository.swift
//  User
//
//  Created by Andi Septiadi on 29/11/21.
//

import Foundation

import RxSwift

public protocol UserRepositoryProtocol {
    func getUser() -> Observable<UserModel?>
}

public class UserRepository: NSObject {
    
    fileprivate let disposeBag: DisposeBag = DisposeBag()
    
    static let shared: UserRepository = UserRepository()
}

extension UserRepository: UserRepositoryProtocol {
    public func getUser() -> Observable<UserModel?> {
        return Observable.create { observer -> Disposable in
            UserManager.getUser()
                .map({ UserMapper.mapToModel(from: $0) })
                .subscribe(onNext: { model in
                    observer.onNext(model)
                    observer.onCompleted()
                })
                .disposed(by: self.disposeBag)
            return Disposables.create()
        }
    }
}
