//
//  UserMapper.swift
//  User
//
//  Created by Andi Septiadi on 29/11/21.
//

import UIKit

final class UserMapper {
    static func mapToModel(from dictionary: [String: Any?]) -> UserModel {
        let name: String = dictionary["name"] as? String ?? ""
        let email: String = dictionary["email"] as? String ?? ""
        var image: UIImage?
        
        if let data = dictionary["image"] as? Data {
            image = UIImage(data: data)
        }
        
        return UserModel(name: name, email: email, image: image ?? UIImage())
    }
    
    static func mapToDictionary(from model: UserModel) -> [String: Any?] {
        return [
            "name": model.name,
            "email": model.email,
            "image": model.image.jpegData(compressionQuality: 1.0)
        ]
    }
}
