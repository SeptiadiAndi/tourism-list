//
//  UserManager.swift
//  User
//
//  Created by Andi Septiadi on 26/11/21.
//

import Foundation
import RxSwift

private let key: String = "user"

public class UserManager {
    
    public static var isEmpty: Bool {
        return UserDefaults.standard.object(forKey: key) == nil
    }
    
    public static func set(with dictionary: [String: Any?]) {
        UserDefaults.standard.setValue(dictionary, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    public static func getUser() -> Observable<[String: Any?]> {
        return Observable.create { observer -> Disposable in
            if let user = UserDefaults.standard.object(forKey: key) as? [String: Any?] {
                observer.onNext(user)
                observer.onCompleted()
            } else {
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    public static func setDefaultUser() {
        let user: UserModel = UserModel(
            name: "Andi Septiadi",
            email: "septiadi70@gmail.com",
            image: UIImage(named: "profile") ?? UIImage()
        )
        UserManager.set(with: UserMapper.mapToDictionary(from: user))
    }
}
