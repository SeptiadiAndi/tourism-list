//
//  TabBarViewController.swift
//  Menu
//
//  Created by Andi Septiadi on 26/11/21.
//

import UIKit
import Common
import Place
import User

public class TabBarViewController: UITabBarController {
    
    lazy var homeNavController: UINavigationController = {
        let viewController = Injection.shared.resolve(HomeViewController.self)!
        let tabBarItem = BarItem(
            itemImage: UIImage(named: "ic-home"),
            itemTitle: "home_tab".localized(bundle: Bundle.menuBundle),
            itemTag: 0
        )
        let navController = MainNavigationViewController(
            rootViewController: viewController,
            tabBarItem: tabBarItem.tabBarItem
        )
        return navController
    }()

    lazy var favoriteNavController: UINavigationController = {
        let viewController = Injection.shared.resolve(FavoriteViewController.self)!
        let tabBarItem = BarItem(
            itemImage: UIImage(named: "ic-favorite-24"),
            itemTitle: "favorite_tab".localized(bundle: Bundle.menuBundle),
            itemTag: 1
        )
        let navController = MainNavigationViewController(
            rootViewController: viewController,
            tabBarItem: tabBarItem.tabBarItem
        )
        return navController
    }()

    lazy var aboutNavController: UINavigationController = {
        let viewController = Injection.shared.resolve(AboutViewController.self)!
        let tabBarItem = BarItem(
            itemImage: UIImage(named: "ic-profile"),
            itemTitle: "about_tab".localized(bundle: Bundle.menuBundle),
            itemTag: 2
        )
        let navController = MainNavigationViewController(
            rootViewController: viewController,
            tabBarItem: tabBarItem.tabBarItem
        )
        return navController
    }()
    
    private var tabItems: [UIViewController] {
        return [homeNavController, favoriteNavController, aboutNavController]
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        configTabBar()
    }

}

// MARK: - Helpers

extension TabBarViewController {
    private func configTabBar() {
        tabBar.barTintColor = .white
        tabBar.backgroundColor = .white
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.mainColor
        tabBarItem.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0, weight: .medium)
        ], for: .normal)
        viewControllers = tabItems
        delegate = self
    }
}

// MARK: - UITabBarControllerDelegate

extension TabBarViewController: UITabBarControllerDelegate {
    public func tabBarController(
        _ tabBarController: UITabBarController,
        shouldSelect viewController: UIViewController
    ) -> Bool {
        return true
    }
}
