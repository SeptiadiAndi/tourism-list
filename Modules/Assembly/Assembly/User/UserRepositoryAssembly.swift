//
//  UserRepositoryAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import User

public class UserRepositoryAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(UserRepository.self) { _ in
            return UserRepository()
        }
    }
}
