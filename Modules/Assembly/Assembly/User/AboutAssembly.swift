//
//  AboutAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 29/11/21.
//

import Swinject
import User

public class AboutAssembly: Assembly {
    
    public init() {}
    
    public func assemble(container: Container) {
        container.register(AboutInteractor.self) { resolver in
            let repository = resolver.resolve(UserRepository.self)!
            return AboutInteractor(repository: repository)
        }
        container.register(AboutPresenter.self) { resolver in
            let useCase = resolver.resolve(AboutInteractor.self)!
            return AboutPresenter(useCase: useCase)
        }
        container.register(AboutViewController.self) { resolver in
            let presenter = resolver.resolve(AboutPresenter.self)!
            return AboutViewController(presenter: presenter)
        }
    }
}
