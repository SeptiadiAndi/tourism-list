//
//  MenuAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import Menu

public class MenuAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(TabBarViewController.self) { _ in
            return TabBarViewController()
        }
    }
}
