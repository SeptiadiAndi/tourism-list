//
//  DetailAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import Place

public class DetailAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(DetailInteractor.self) { resolver in
            let repository = resolver.resolve(TourismRepository.self)!
            return DetailInteractor(repository: repository)
        }
        container.register(DetailRouter.self) { _ in
            return DetailRouter()
        }
        container.register(DetailPresenter.self) { resolver in
            let useCase = resolver.resolve(DetailInteractor.self)!
            let router = resolver.resolve(DetailRouter.self)!
            return DetailPresenter(useCase: useCase, router: router)
        }
        container.register(DetailViewController.self) { resolver, place in
            return DetailViewController(
                tourism: place,
                presenter: resolver.resolve(DetailPresenter.self)!
            )
        }
    }
}
