//
//  LocationAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 01/12/21.
//

import Foundation
import Swinject
import Place

public class LocationAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(LocationInteractor.self) { resolver in
            let repository = resolver.resolve(TourismRepository.self)!
            return LocationInteractor(repository: repository)
        }
        container.register(LocationRouter.self) { _ in
            return LocationRouter()
        }
        container.register(LocationPresenter.self) { resolver in
            let useCase = resolver.resolve(LocationInteractor.self)!
            let router = resolver.resolve(LocationRouter.self)!
            return LocationPresenter(useCase: useCase, router: router)
        }
        container.register(LocationViewController.self) { resolver, place in
            return LocationViewController(
                placeId: place,
                presenter: resolver.resolve(LocationPresenter.self)!
            )
        }
    }
}
