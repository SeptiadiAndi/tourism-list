//
//  PlaceRepositoryAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import Place
import Common

public class TourismRepositoryAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(TourismRepository.self) { _ in
            let remote = PlaceRemoteDataSource()
            let local = PlaceLocalDataSource(realm: RealmManager.shared.realm)
            let mapper = ListPlaceMapper()
            return TourismRepository
                .shared(local, remote, mapper)
        }
    }
}
