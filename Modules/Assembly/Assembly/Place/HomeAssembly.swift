//
//  HomeAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import Place

public class HomeAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(HomeInteractor.self) { resolver in
            let repository = resolver.resolve(TourismRepository.self)!
            return HomeInteractor(repository: repository)
        }
        container.register(HomeRouter.self) { _ in
            return HomeRouter()
        }
        container.register(HomePresenter.self) { resolver in
            let presenter = HomePresenter(
                useCase: resolver.resolve(HomeInteractor.self)!,
                router: resolver.resolve(HomeRouter.self)!
            )
            return presenter
        }
        container.register(HomeViewController.self) { resolver in
            let controller = HomeViewController(
                nibName: "HomeViewController",
                bundle: Bundle.placeBundle
            )
            controller.presenter = resolver.resolve(HomePresenter.self)!
            return controller
        }
    }
}
