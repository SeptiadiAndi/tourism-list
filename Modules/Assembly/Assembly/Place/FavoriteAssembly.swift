//
//  FavoriteAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import Place

public class FavoriteAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(FavoriteInteractor.self) { resolver in
            let repository = resolver.resolve(TourismRepository.self)!
            return FavoriteInteractor(repository: repository)
        }
        container.register(FavoriteRouter.self) { _ in
            return FavoriteRouter()
        }
        container.register(FavoritePresenter.self) { resolver in
            let presenter = FavoritePresenter(
                useCase: resolver.resolve(FavoriteInteractor.self)!,
                router: resolver.resolve(FavoriteRouter.self)!
            )
            return presenter
        }
        container.register(FavoriteViewController.self) { resolver in
            return FavoriteViewController(presenter: resolver.resolve(FavoritePresenter.self)!)
        }
    }
}
