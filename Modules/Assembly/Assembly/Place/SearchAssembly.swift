//
//  SearchAssembly.swift
//  Assembly
//
//  Created by Andi Septiadi on 30/11/21.
//

import Foundation
import Swinject
import Place

public class SearchAssembly: Assembly {
    public init() {}
    
    public func assemble(container: Container) {
        container.register(SearchInteractor.self) { resolver in
            let repository = resolver.resolve(TourismRepository.self)!
            return SearchInteractor(repository: repository)
        }
        container.register(SearchRouter.self) { _ in
            return SearchRouter()
        }
        container.register(SearchPresenter.self) { resolver in
            return SearchPresenter(
                useCase: resolver.resolve(SearchInteractor.self)!,
                router: resolver.resolve(SearchRouter.self)!
            )
        }
        container.register(SearchViewController.self) { resolver in
            return SearchViewController(
                presenter: resolver.resolve(SearchPresenter.self)!
            )
        }
    }
}
