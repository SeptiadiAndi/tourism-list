//
//  SearchUseCaseTests.swift
//  TourismListAppTests
//
//  Created by Andi Septiadi on 08/11/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Tourism_List
class SearchUseCaseTests: XCTestCase {
    
    static var placeIdInput = 1
    static var searchInput = "Mountain"
    static var tourismModel = TourismModel(id: 1, name: "Mountain")

    func testSearchTourismPlaces() throws {
        let useCase = SearchInteractorMock()
        
        do {
            let result = try useCase
                .searchTourismPlaces(searchText: SearchUseCaseTests.searchInput)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, [SearchUseCaseTests.tourismModel])
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        let useCase = SearchInteractorMock()
        
        do {
            let result = try useCase
                .updateFavoritedTourism(placeId: SearchUseCaseTests.placeIdInput)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, SearchUseCaseTests.tourismModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
}

extension SearchUseCaseTests {
    class SearchInteractorMock: SearchUseCase {
        var functionWasCalled = false
        
        func searchTourismPlaces(searchText text: String) -> Observable<[TourismModel]> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext([SearchUseCaseTests.tourismModel])
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int) -> Observable<TourismModel> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(SearchUseCaseTests.tourismModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
