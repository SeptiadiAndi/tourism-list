//
//  AboutUseCaseTests.swift
//  TourismListAppTests
//
//  Created by Andi Septiadi on 08/11/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Tourism_List
class AboutUseCaseTests: XCTestCase {
    
    static var userModel = UserModel(
        name: "Andi",
        email: "septiadi70@gmail.com",
        image: UIImage()
    )

    func testGetUser() throws {
        let useCase = AboutInteractorMock()
        
        do {
            let result = try useCase
                .getUserProfile()
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, AboutUseCaseTests.userModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }

}

extension AboutUseCaseTests {
    class AboutInteractorMock: AboutUseCase {
        var functionWasCalled = false
        
        func getUserProfile() -> Observable<UserModel?> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(AboutUseCaseTests.userModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
