//
//  FavoriteUseCaseTests.swift
//  TourismListAppTests
//
//  Created by Andi Septiadi on 08/11/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Tourism_List
class FavoriteUseCaseTests: XCTestCase {
    
    static var placeIdInput = 1
    static var tourismModel = TourismModel(id: 1, name: "Mountain")

    func testGetFavoritedTourismList() throws {
        let useCase = FavoriteInteractorMock()
        
        do {
            let result = try useCase
                .getFavoritedTourismList()
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, [FavoriteUseCaseTests.tourismModel])
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        let useCase = FavoriteInteractorMock()
        
        do {
            let result = try useCase
                .updateFavoritedTourism(
                    placeId: FavoriteUseCaseTests.placeIdInput
                )
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, FavoriteUseCaseTests.tourismModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
}

extension FavoriteUseCaseTests {
    class FavoriteInteractorMock: FavoriteUseCase {
        var functionWasCalled = false
        
        func getFavoritedTourismList() -> Observable<[TourismModel]> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext([FavoriteUseCaseTests.tourismModel])
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int) -> Observable<TourismModel> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(FavoriteUseCaseTests.tourismModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
