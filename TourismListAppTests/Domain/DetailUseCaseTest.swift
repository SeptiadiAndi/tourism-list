//
//  DetailUseCaseTest.swift
//  TourismListAppTests
//
//  Created by Andi Septiadi on 08/11/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Tourism_List
class DetailUseCaseTest: XCTestCase {

    static var placeIdInput = 1
    static var tourismModel = TourismModel(id: 1, name: "Mountain")

    func testGetTourismList() throws {
        let useCase = DetailInteractorMock()
        
        do {
            let result = try useCase
                .getTourismPlace(placeId: DetailUseCaseTest.placeIdInput)
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, DetailUseCaseTest.tourismModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        let useCase = DetailInteractorMock()
        
        do {
            let result = try useCase
                .updateFavoritedTourism(
                    placeId: DetailUseCaseTest.placeIdInput
                )
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, DetailUseCaseTest.tourismModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }

}

extension DetailUseCaseTest {
    class DetailInteractorMock: DetailUseCase {
        var functionWasCalled = false
        
        func getTourismPlace(placeId: Int) -> Observable<TourismModel?> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(DetailUseCaseTest.tourismModel)
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int) -> Observable<TourismModel> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(DetailUseCaseTest.tourismModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
