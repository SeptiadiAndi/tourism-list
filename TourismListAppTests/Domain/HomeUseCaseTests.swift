//
//  HomeUseCaseTests.swift
//  TourismListAppTests
//
//  Created by Andi Septiadi on 08/11/21.
//

import XCTest
import RxSwift
import RxBlocking

@testable import Tourism_List
class HomeUseCaseTests: XCTestCase {
    
    static var placeIdInput = 1
    static var tourismModel = TourismModel(id: 1, name: "Mountain")

    func testGetTourismList() throws {
        let useCase = HomeInteractorMock()
        
        do {
            let result = try useCase
                .getTourismList()
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, [HomeUseCaseTests.tourismModel])
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }
    
    func testUpdateFavoritedTourism() throws {
        let useCase = HomeInteractorMock()
        
        do {
            let result = try useCase
                .updateFavoritedTourism(
                    placeId: HomeUseCaseTests.placeIdInput
                )
                .toBlocking()
                .first()
            
            XCTAssert(useCase.verify())
            
            XCTAssertEqual(result, HomeUseCaseTests.tourismModel)
        } catch let error {
            XCTAssertThrowsError(error)
        }
    }

}

extension HomeUseCaseTests {
    class HomeInteractorMock: HomeUseCase {
        var functionWasCalled = false
        
        func getTourismList() -> Observable<[TourismModel]> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext([HomeUseCaseTests.tourismModel])
                return Disposables.create()
            }
        }
        
        func updateFavoritedTourism(placeId: Int) -> Observable<TourismModel> {
            functionWasCalled = false
            return Observable.create { observer -> Disposable in
                self.functionWasCalled = true
                observer.onNext(HomeUseCaseTests.tourismModel)
                return Disposables.create()
            }
        }
        
        func verify() -> Bool {
            return functionWasCalled
        }
    }
}
